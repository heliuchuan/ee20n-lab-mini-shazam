function match = findMatch(query_features,songs, print_len, thresh)
% Finds a song in 'songs' that has a sequence of features that is within a
% bit error rate of 'thresh' from the query features, and returns 
% the name of the match.
% 
% Inputs:
% queryFeatures - a 100x1 vector of 16-bit decimal numbers that represent
%       the features of the noisy query clip
% songs - a struct array of all the songs in the database
% thresh - bit error rate
%
% Outputs:
% match - name of the closest match in the database to the query prints
match = '';
oldrate = 1;
for x = 1:100
    feat = query_features(x);
    for y = 1:length(songs)
        newsong = songs(y).features - feat;
        newsong = and(newsong,newsong);
        newsong = ~newsong;
        indicies = find(newsong);
        if length(indicies) ~= 0
            for v = 1:length(indicies)
                if (indicies(v)+100-x) < length(songs(y).features) & (indicies(v)-x+1) > 0
                    rate = ber(query_features,songs(y).features(indicies(v)-x+1:indicies(v)-x+100),print_len);
                    if rate <= thresh
                        if rate < oldrate
                            oldrate = rate;
                            match = songs(y).name;
                        end
                    end
                end
            end
        end
    end    
end        
             
        
        
            
            





