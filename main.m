% Main Shazam File

% constants
window_time = 0.37; % duration (s) of each window
fs_target = 4000;   % desired sampling rate - 4kHz
print_len = 16;     % bits in a subfingerprint
SNR = 15;
demo_music = 0;   % set this to 1 if you want to hear the music you're processing
thresh = .4;
u = 20;
numbercor = 0;


%% Database Prep
audio_directory = 'data';   % directory with test data
disp('Generating the database .... ')
% find all the files in the data directory
filelist = getFiles(audio_directory);   

% extract features from each song and store them in a data structure of
% Songs (class with a name and a list of subfingerprints)
songs = makeDatabase(filelist,window_time,fs_target,print_len);
disp('Database constructed. ');



for q = 1:u
    %% Query Generation
    disp('Generating query sample ... ')

    % generate a noisy query
    randsong = randi(length(songs));
    query_filename = songs(randsong).name; % change this to a random filename from the list
    disp('Our song selected is')
    query_filename
    [query_features clean noisy SR] = generateQuery(query_filename,SNR,window_time,fs_target,print_len);


    %% Query playback
    if demo_music
        % Listen to the difference in sound quality
        disp('First the clean Version... ')
        playAudio(3*clean,SR,5);
        pause(1)
        disp('Then the noisy Version... ') 
        playAudio(noisy,SR,5);
    end

%% Query search
    match = 0;
    disp('Searching ... ')
    match1 = findMatch(query_features,songs,print_len,thresh);
    disp('The match found is')
    match = match1
    numbercor = numbercor + strcmp(match,query_filename);
end
ratecorrect = numbercor/u;
ratecorrect
% Now check if the match is correct