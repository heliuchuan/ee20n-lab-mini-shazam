%% Database Prep
audio_directory = 'data';   % directory with test data
disp('Generating the database .... ')
% find all the files in the data directory
filelist = getFiles(audio_directory);   

% extract features from each song and store them in a data structure of
% Songs (class with a name and a list of subfingerprints)
songs = makeDatabase(filelist,window_time,fs_target,print_len);
disp('Database constructed. ');