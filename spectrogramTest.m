% spectrogramTest.m

%% Part 1: Frequency-domain analysis of testAudio and scrambledAudio

% load and listen to the signals

% find the DFT of testAudio and scrambledAudio

% plot the DFT of testAudio and scrambledAudio 


%% Part 2: Find spectrograms of the two signals

window = 256;
NFFT = 256;
noverlap = 250;

figure(1) % plot the spectrogram of testAudio


figure(2) % plot the spectrogram of scrambledAudio



%% Part 3: Find the mystery signal that generates the spectrogram

fs = 1000;

figure(3) % test out your signal by plotting the spectrogram