close all

%% Part 1: Signal creation
% Put your time and signal vectors here
dr = .1;
t = 0:.1:2*pi;
x=sin(t);
t2 = 0:.1:5.5
xhat=sin(t2);


%% Part 2: Time-domain plots
figure(1)
hold on 
% plot stuff here
 plot(t,x);
 plot(t2,xhat,'r')
% legend('X','Xhat','Windowed Xhat')
% uncomment the line above this when you're done

%% Part 3: find DFT of signals

% first find the DFT of x, xhat, and xhatw
A = dftmtx(63);
dftx = x * A
absdftx = abs(dftx)
plot(absdftx);



%% Part 4: frequency domain plots
figure(2)
hold on
%then plot the magnitude of each DFT 


%legend('X','Xhat','Windowed Xhat')
% uncomment the line above this when you're done
